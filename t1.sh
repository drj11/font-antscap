#!/bin/sh

isvg () {
    rsvg-convert | kitty icat
}

T="
EXPRESSWAY
INDIVIDUALS
IRONWORKS
KINESIOLOGY
PHILANDERING
PROFESSIONALLY
STRINGBEAN
SYNERGISTIC
UNANSWERED
UNBARNBURN
$@"

while [ $# != 0 ]
do
  case "$1 $2" in
    ("-t g") T="
BREAKTHROUGH
DISCOGRAPHY
ENGAGEMENTS
FILMGOERS
GARGOYLES
GYNAECOLOGIST
ORGANIZATION
RECOGNIZING
SONGWRITING
" ; shift 2 ;;
    ("-t k") T="
ACKNOWLEDGED
DJOKOVIC
KYRGYZSTAN
SHAKESPEARE
THANKSGIVING
" ; shift 2 ;;
    (*) break ;;
  esac
done

Render=isvg
if [ "$1" = - ]
then
  shift
  Render=cat
fi

Font=SmartAntscap.otf

(
for t in $T
do
printf '%s' "$t" |
hb-view --output-format svg $Font
done
) |
~/prj/gley/gley |
$Render
