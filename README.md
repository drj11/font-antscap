# Antscap

maybe aka _Assette C_.

A font modelled after
the way tape loops around wheels and reels and capstans (anagram: Antscap).

It is also a matrix in which i can develop OpenType features,
using AFDKO feature syntax.

![The upper case A to Z in a random order](antscap-uc-plaque.png "Antscap Regular upper case alphabet")


## Design notes: graphics

A classic 700/100 system, fairly rigidly and geometrically imposed.

**O** is _exactly_ 700 high, perfect circle, no overshoot.
Stroke width is _exactly_ 100.

The dot, in **A**, **I**, and various others, is a circle of
diameter 200.

Sidebearings are generally 0 unless there is a long vertical
stroke on that side, in which case 50.

**S** and **Z** model two sizes of implicit capstan.
The **S** has a bend of diameter 200 on the inside of the stroke
(with the outside of the stroke having a bend diameter of 400).
The **Z** has a tighter bend, diameter 100 on the inside of the
stroke, and 300 on the outside of the stroke.

Those curves are used repeatedly and frequent throughout the
alphabet.


## Design notes: features

This is a feature heavy font.

Many letters are designed in alternate sets that have terminals
that either face vertically (or neutrally) or face outward.
Those alternates are designed to be used so that terminals can
be automatically adjusted to face each other in adjacent letters.
This give the impression of a continuous (or somewhat
continuous) piece of tape looping across many letters.

A naming system helps manage the chaotic complexity.
Terminals generally appear in one of four corners, a two letter
code is used to identify the position:
`bl` bottom left; `br` bottom right; `tl` top left; `tr` top right.

This system is used in both glyph names and in glyph group names.
Glyph names are defined in the font source (Glyphs.app in this
case) and _used_ by the feature files; they also appear in the
binary font file.
Glyph group names are defined and used in the feature file and
are entirely internal to the feature file, they do not appear in
the binary font file.

When a glyph has alternates that have vertical and
facing-outward alternative terminals, the form with terminals
that do not face outward is considered the neutral form.
An alternate with a terminal that faces out will have a _dot
extension_ with the 2-letter code for that terminal.
In the case of multiple possibilities (**N** **K**), the
2-letter codes are appended in the order given above (which, for
the nerds who are reading, row major, sorted from bottom to top
and left to right; increasing in y- and x-axes).

Thus we have **K** and **K.brtr**, the second one being the K
with both terminals on the right facing outward.
We have **N**, **N.bl**, **N.bltr**, **N.tr**; N has two
terminals that can face outward and all combinations are used.


## Building

The letterforms themselves are made in Glyphs,
exporting to an OTF file.
This OTF file is only intended to be used for its letterforms.
GPOS and GSUB rules will be in AFDKO feature files.
Kerning is expected to be small enough in scope that it can be
hand-written.

The supplied `Makefile` will let
you build the final OTF file from
the pre-feature OTF file and the feature file:

    make

This script relies on various tools being installed in
particular places.
If you have Unix skills you can probably work out how to
make it work on your system if you `cat Makefile`.


## Connecting principle

Adjacent glyphs automagically connect to each other,
to make longer connecting lengths of tape.

Many letters have a corner terminal that can be turned towards
the adjacant glyph, or away (to the vertical, generally).
Some letters have alternate forms that have different terminals.
For example **S** has terminals at top-right and bottom-left that
face adjacent glyphs,
it has a reversed alternate that
has terminals at top-left and bottom-right.
**I** has a bottom terminal that is vertical, and
two alternates, one that faces left, one that faces right.

I suppose in principle a glyph may have up to 3^n alternates,
where n is the number of terminals (up to 4, for X).
Most terminals will have only 1 or 2 forms; 3 is for i, which
has a bottom terminal that can point one of three ways.


# END
